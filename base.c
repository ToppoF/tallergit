#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "estudiantes.h"


float desvStd(estudiante curso[]){
	int j;
	float med=0,des=0,var=0;

	for(j=0;j<24;j++){
		med=med+curso[j].prom;        //En esta funcion recorro 3 veces las notas para ir sacando por parte las Desviacion
	}
	med=med/24;
	for(j=0;j<24;j++){
		var=var+pow(curso[j].prom-med,2);
	}
	des=pow(var/24,0.5);
	printf("\nDesviacion:%d",des);
	return des;
}

float menor(float prom[]){          //En esta funcion recorro las notas y comparo primeramente con un 7 para luego ir sacando la nota menor
	float proM=7.0,i;
	for (i=0;i<24;i++){
		if (curso[i].prom<proM){
			proM=curso[i].prom;
		}
	}
	printf("\nNota menor:%d",proM);
	return proM;
}

float mayor(float prom[]){					//Aqui repetimos la misma accion que en la funcion anterior de forma contraria partiendo del 1 para sacar la nota mayor
	float promA=1.0,i;
		for (i=0;i<24;i++){
			if (curso[i].prom>promA){
				promA=curso[i].prom;
			}
		}
		printf("\nNota mayor:%d",promA);
		return promA;
}
void registroCurso(estudiante curso[]){			//En registroCurso agrego una matriz x24 para luego recorrerla con un for y de esta forma el usuario agrega las notas de los alumnos enlistados
	int i;										// paralelamente tambien saca los promedios de cada uno
	int prom[24];
	int promP;

	for (i=0;i<24;i++){
	curso[i].prom=0;

		printf("Agregue notas del estudiante %s %s %s:\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM);

		printf("Ingrese nota de proyecto 1\n");
		scanf("%f",&curso[i].asig_1.proy1);
		promP=promP+(curso[i].asig_1.proy1*0.2);

		printf("Ingrese nota de proyecto 2\n");
		scanf("%f",&curso[i].asig_1.proy2);
		promP=promP+(curso[i].asig_1.proy2*0.2);

		printf("Ingrese nota de proyecto 3\n");
		scanf("%f",&curso[i].asig_1.proy3);
		promP=promP+(curso[i].asig_1.proy3*0.3);

		printf("Ingrese nota de control 1\n");
		scanf ("%f",&curso[i].asig_1.cont1);
		promP=promP+(curso[i].asig_1.cont1*0.05);

		printf("Ingrese nota de control 2\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		promP=promP+(curso[i].asig_1.cont2*0.05);

		printf("Ingrese nota de control 3\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		promP=promP+(curso[i].asig_1.cont3*0.05);

		printf("Ingrese nota de control 4\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		promP=promP+(curso[i].asig_1.cont4*0.05);

		printf("Ingrese nota de control 5\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		promP=promP+(curso[i].asig_1.cont5*0.05);

		printf("Ingrese nota de control 6\n");
		scanf ("%f",&curso[i].asig_1.cont2);
		promP=promP+(curso[i].asig_1.cont6*0.05);

		prom[i]=promP;
		}
}

void clasificarEstudiantes(char path[], estudiante curso[]){ // En esta fucion se abren 2 txt para luego separar y agregar los estudiantes aprovados y reprovados a los respectivos archivos  
	FILE *apr;
	FILE *rep;

	char aprovados[1024]=""
	char reprovados[1024]="" 

	int i=0;
	int jaus=1;

	apr=fopen("aprobados.txt","w");
	rep=fopen("reprobados.txt","w");

	while(jaus==1){
		if (curso[i].prom != 0.0){
			break;
		}
		if (curso[i].prom<=3.9 && curso[i].prom != 0){
			strcat(reprobados ,curso[i].nombre);
			strcat(reprobados, " ");
			strcat(reprobados ,curso[i].apellidoP);
			strcat(reprobados, " ");
			strcat(reprobados, curso[i].apellidoM);
			strcat(reprobados, " ");
			fprintf(rep,"%s %f\n",reprobados,curso[i].prom);
			strcpy(aprobados,"");
		}
		else{
			strcat(aprobados ,curso[i].nombre);			
			strcat(aprobados, " ");			
			strcat(aprobados ,curso[i].apellidoP);
			strcat(aprobados, " ");
			strcat(aprobados, curso[i].apellidoM);
			strcat(aprobados, " ");
			fprintf(apr, "%s %f\n",aprobados,curso[i].prom);
			strcpy(reprobados,"");
	}
}


void metricasEstudiantes(estudiante curso[]){  //Esta funcion muestra lo calculado anteriormente en las funciones mayor menor y desvStd 

	float des=desvStd(curso[]),promA=mayor(curso[]),proM=menor(curso[]);
	
	prinf("\nLas metricas de disperción son:");
	printf("\nLa desviacion estandar es: %f\n El mejor promedio fue:%f\nEl menor promedio fue:%f\n",des,proA,proM);
}

void menu(estudiante curso[]){
	int opcion;
    do{
    	printf("\nEste programa permite sacar promedios \ny analiza las notas de los estudiantes");
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}
